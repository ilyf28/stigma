const elixir = require('laravel-elixir');

require('laravel-elixir-webpack-ex');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(mix => {
    mix.sass('app.scss');
    //    .webpack('app.js');

    mix.copy('node_modules/bootstrap/fonts', 'public/build/fonts');
    mix.copy('node_modules/font-awesome/fonts', 'public/build/fonts');

    mix.webpack(
        {
            main: 'main.ts',
            polyfills: 'polyfills.ts',
            vendor: 'vendor.ts'
        },
        require('./webpack.config.js'),
        'public/dist',
        'resources/assets/typescript'
    );

    mix.version([
        'css/app.css',
        'dist/main.js',
        'dist/polyfills.js',
        'dist/vendor.js'
    ]);
});
