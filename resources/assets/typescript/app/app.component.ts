import { Component } from '@angular/core';
import { correctHeight, detectBody } from './app.helpers';

// Main CSS files
import '../assets/styles/style.css';

declare var jQuery:any;

@Component({
    selector: 'app',
    template: require('./app.template.html')
})

export class AppComponent {
    ngAfterViewInit() {
        // Run correctHeight function on load and resize window event
        jQuery(window).bind("load resize", function() {
            correctHeight();
            detectBody();
        });

        // Correct height of wrapper after metisMenu animation.
        jQuery('.metismenu a').click(() => {
            setTimeout(() => {
                correctHeight();
            }, 300)
        });
    }
}