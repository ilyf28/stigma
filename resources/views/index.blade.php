<!DOCTYPE html>
<html lang="en">
    <head>
        <base href="/">
        <meta charset="utf-8">
        <title>Getting Started with Webpack</title>
        <link rel="stylesheet" type="text/css" href="{{ elixir('css/app.css') }}">
    </head>
    <body class="pace-done">
        <app></app>
        <script type="text/javascript" src="{{ elixir('dist/polyfills.js') }}"></script>
        <script type="text/javascript" src="{{ elixir('dist/vendor.js') }}"></script>
        <script type="text/javascript" src="{{ elixir('dist/main.js') }}"></script>
    </body>
</html>